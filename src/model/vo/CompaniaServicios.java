package model.vo;


public class CompaniaServicios implements Comparable<CompaniaServicios> {
	
	private String nomCompania;
	
	private Service[] servicios;

	public String getNomCompania() {
		return nomCompania;
	}

	public void setNomCompania(String nomCompania) {
		this.nomCompania = nomCompania;
	}

	public Service[] getServicios() {
		return servicios;
	}

	public void setServicios(Service[] servicios) {
		this.servicios = servicios;
	}

	@Override
	public int compareTo(CompaniaServicios o) {
		// TODO Auto-generated method stub
		int compare = -1;
		if(nomCompania.equals(o.getNomCompania())){
			compare = 0;
		}
		else if(nomCompania.compareTo(o.getNomCompania()) > 0) {
			return 1;
		}
		return compare;
	}
	
	

}
